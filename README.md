# install needed packages 
sudo apt install qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils virtinst libvirt-daemon

# enable libvirt deamon 
sudo systemctl enable libvirtd 

# get image iso 
cd /var/lib/libvirt/images && wget $image_iso_link

# lanch the vm 
we can use virt-manager 

